import React from 'react'

const Index = (props) => {
    return (
        <table>
            <thead>
                <tr>
                    <th>Nombre Completo</th>
                    <th>Email</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                {props.users.length > 0 ? (
                    props.users.map(user => (
                        <tr key={user.id}>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            <td>
                                <button className="button muted-button"
                                    onClick={
                                        () => { props.editRow(user) }
                                    }>Editar</button>
                                <button className="button muted-button"
                                    onClick={() => { props.deleteUser(user.id) }}>
                                    Eliminar</button>
                            </td>
                        </tr>
                    ))
                ) : (
                        <tr>
                            <td colSpan={3}>No hay Usuarios registrados</td>
                        </tr>
                    )}
            </tbody>
        </table>
    )
}

export default Index